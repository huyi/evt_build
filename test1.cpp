#include <iostream>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include <vector>
#include "DAC.h"
#include "island.h"
#include "Evt_build.h"
using namespace DAC;
using namespace std;


int main(){
    //test digit
    /*
    int i = 112,j=1321;
    double x=0,y=0;
    toPOS(i,j,x,y);
    std::cout<<x<<" "<<y<<std::endl;
    toID(i,j,x,y);
    std::cout<<i<<" "<<j<<std::endl;
    */
    //test Evt_build
       // Declaration of leaf types
    //vector<int>     *valid;
    vector<int>     *chipID   = new std::vector<Int_t>();
    vector<int>     *timeFPGA = new std::vector<Int_t>();
    vector<int>     *timeChip = new std::vector<Int_t>();
    vector<int>     *row      = new std::vector<Int_t>();
    vector<int>     *col      = new std::vector<Int_t>();

   // List of branches
    //TBranch        *b_valid;   //!
    TBranch        *b_chipID  ;   //!
    TBranch        *b_timeFPGA;   //!
    TBranch        *b_timeChip;   //!
    TBranch        *b_row     ;   //!
    TBranch        *b_col     ;   //!

    Int_t t_eventID;
    std::vector<Int_t> t_chipID;
    std::vector<Double_t> t_x;
    std::vector<Double_t> t_y;

    //Int_t t_numOfCluster=0;
    Int_t t_numOfHits=0;
    //std::vector<Double_t> t_x_cluster;
    //std::vector<Double_t> t_y_cluster;

    std::vector<long long> t_time;


    string ip = str1+str3+to_string(2)+".root";
    string op = str2+str3+to_string(2)+".root";
    TFile *input = TFile::Open("../../Run0579-Rundata-analyze-02.root");
    //TFile *input = TFile::Open(ip.c_str());
    TTree *t1 = (TTree *)input->Get("HitsInfo");

    t1->SetBranchAddress("chipID", &chipID);
    t1->SetBranchAddress("timeFPGA", &timeFPGA);
    t1->SetBranchAddress("timechip", &timeChip);
    t1->SetBranchAddress("row", &row);
    t1->SetBranchAddress("col", &col);

    TFile *output = TFile::Open("../../test.root", "RECREATE"); // output
    //TFile *output = TFile::Open(op.c_str(), "RECREATE"); // output
    TTree *t2 = new TTree("test", "title");

    t2->Branch("eventID", &t_eventID);
    t2->Branch("chipID", &t_chipID);
    t2->Branch("x", &t_x);
    t2->Branch("y", &t_y);
    //t2->Branch("x_cluster", &t_x_cluster);
    //t2->Branch("y_cluster", &t_y_cluster);
    //t2->Branch("numOfCluster", &t_numOfCluster);
    t2->Branch("numOfHits", &t_numOfHits);
    t2->Branch("time", &t_time);

    Evt_build* e_build = new Evt_build();
    for(Int_t i = 0; i < t1->GetEntries(); i++){
        t1->GetEntry(i);
        for(int j=0;j<64;j++){
            if(e_build->judge((*timeFPGA)[j],(*timeChip)[j])){
                e_build->generate(t_eventID,t_chipID,t_x,t_y,t_time);
                t_numOfHits = t_x.size();
                cout<<"Hits: "<<t_numOfHits<<endl; //<<"     Cluster: "<<t_numOfCluster<<endl;
                t2->Fill();
            }
            e_build->fill((*chipID)[j],(*row)[j],(*col)[j]);
        }      
    }
    e_build->generate(t_eventID,t_chipID,t_x,t_y,t_time);
    t2->Fill();
    delete e_build;

    output->cd();
    t2->Write();
    output->Close();

    
    return 0;
}


