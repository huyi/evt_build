#ifndef _Evt_build_H
#define _Evt_build_H

#include <iostream>
#include <vector>
using namespace std;

class Evt_build{
private:
    int time_crit_chip = 0;//CLOCK
    long long time_crit_FPGA = 0;//CLOCK
    long long FPGA_gap = 0;
    //generate event's var
    int evtID=0;
    vector<int> chipID; 
    //vector<int> row;
    //vector<int> col;
    vector<double> x;
    vector<double> y;
    vector<long long> time;
    vector<int> time_chip;

protected:
    long long t_time=0;

public:
    Evt_build();
    ~Evt_build();

    void checkmemory();
    int getsize(){return x.size();}
    bool judge(long long time_FPGA, int time_Chip,pair<long long,int> mintime);
    bool judge(long long time_FPGA, int time_Chip,pair<long long,int> mintime,int bias,int bias2);
    bool judge(int time_FPGA, int time_Chip);//judge whether generate then fill, it will cal the time of entry too
    void fill(int t_chipID, int row, int col,long long time_FPGA,int time_chip);//fill temp var which are privates, time is calculated in judge
    void generate(int& t_evtID,vector<int>& t_chipID,vector<double>& t_x,vector<double>& t_y,vector<long long>& t_time,vector<int>& time_chip);
    void generate(int& t_evtID,vector<int>& t_chipID,vector<double>& t_x,vector<double>& t_y,vector<double>& t_x_cluster,vector<double>& t_y_cluster,int& t_numOfCluster,vector<long long>& t_time);
};
#endif