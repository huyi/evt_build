#ifndef _DAC_h

namespace DAC{

  void toPOS(int i, int j, double &x, double &y) ;

  void toID(int &i, int &j, double x, double y) ;

  long long toTime(long long FPGA, int Chip);
};

#endif // _fun_h
