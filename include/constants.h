#ifndef _constants_H
#define _constants_H

//
// Toy detector specifications
//

const unsigned int nStation = 12;  // Number of detector planes

const double planeX = 560.0;       // Plane X size 
const double planeY = 220.0;       // Plane Y size
const double dzStation = 50.0;     // Distance bet. 2 planes
const double firstZ = 0.001;       // Origin (...should be non-zero...)
const double dist_PV = 150.0;      // Distance bet. primary vertex and first sensor


const double pitch      = 0.2;   // detector pitch
const double resolution = 0.05;  // smear
const double thickness  = 0.00; // sensor thickness



//
// Misalignment scales
//

const double offsetX = 0.1;
const double offsetY = 0.1;
const double offsetZ = 0.1;

const double offsetA = 0.002;
const double offsetB = 0.002;
const double offsetC = 0.002;


//
// MILLEPEDE tuning
//

const bool m_dofs[6] = {1,1,1,1,1,1}; // Degrees of freedom to take into account (Boolean version)

// Individual constraint on alignment constants (set via ParSig in Millepede)

const double m_Sigm[6] = {0.1,0.1,0.1,0.002,0.002,0.002};

// Cuts on residuals (when fitting the tracks) (in mm)
// !!! Adapt them to your situation !!!

const double m_res_cut = 2.;
const double m_res_cut_init = 5.; // Larger for the first iteration (you are misaligned !!!)
const double m_start_chi_cut = 100.;

//pixel_size
const double pixel_size_x = 0.025 ; //unit:mm
const double pixel_size_y = 0.025 ; //unit:mm

//CLOCK
const double CLOCK = 25; //unit:ns
const int FPGA_CLOCK = 1<<8;
const int Chip_CLOCK = 1;

//
const int nrows = 1<<9;
const int ncols = 1<<10;
#endif
