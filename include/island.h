#ifndef ISLAND_H
#define ISLAND_H
#include <vector>
#include <unordered_set>
#include "constants.h"
using namespace std;
class island{
    private:
    int numOfislands = 0;
    vector<double> island_centerx;
    vector<double> island_centery;
    
    //double use
    vector<pair<double,double>> t_island;//pair for temp use
    vector<pair<int,int>> t_island_island;//pair for temp use


    //int use
    //vector<vector<int>> Map = vector<vector<int>>(nrows,vector<int>(ncols,0));
    

    public:
    island();
    island(const vector<double> &x,const vector<double> &y);
    //island(const vector<int> &trow,const vector<int> &tcol);

    ~island();
    int getnumOfislands(){return numOfislands;}
    const vector<double>& getcenterxOfislands(){return island_centerx;}
    const vector<double>& getcenteryOfislands(){return island_centery;}

    //for double
    void dfs(std::unordered_set<int> &t_set,const vector<vector<int>> &id,int i,int j,double &x,double &y,int &n);
    bool inArea(const vector<vector<int>> &id,int i,int j);
    bool inLink(const vector<vector<int>> &id,int i,int j,int ii, int jj);
    
    //for int
    //void dfs();
    //bool inArea();
    
};
#endif


