function eff(){
    root -l -q DeBinary.cpp\(\"$1\"\)
    cd build
    ./test $1
    cd ../clustering
    root -l -q data_cluster.c\(\"$1\"\)
    #cd ../TrackFitMinuit
    #./trackfit $1 
    cd ..
    echo "end Run$1"
}

eff "0251" &
wait
echo "end all"


