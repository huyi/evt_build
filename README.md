# Build Event

    cd build
    rm -rf *
    cmake ..
    make
    ./test 0240  (runID)


## Chip numbers: line 18
    Int_t numOfChip = 4;

## I/O file path: line 48

## Chip initialize detail

### 1.Order of chipID: line 65
     unordered_map<Int_t,Int_t> chip_order = {{0,4},{1,3},{2,2},{3,1}};
   
### 2. Phase of chip: line 66
since we don't use timechip for building event, you can set them to 0

    unordered_map<Int_t,Int_t>  timealign = {{0,28},{1,147},{2,0},{3,92}};

For example, {0,28} means the first chip, whose ID can be set in before, in here its **4**. Its phase is 28.

One simple way to calculate the phase by time alignment file like below:

let max timeChip one chip's phase be **0**,

others' phase = max_timeChip -timeChip

    ChipID      4    3   2   1
    timeChip    204  85  232 140
    Phase       28   147 0   92



# toroot 
  It can transform txt to root, can be run in mutiple cores.

  just change I/O file in **run.sh**

    root -l -q toroot.cpp\(\"input.txt\",\"output.txt\"\)

# Clustering 
It can do clustering after running build event, 

I/O file can be changed in **line 56-62**,

chiporder should be set in it

    unordered_map<Int_t,Int_t> chip_order = {{0,1},{1,2},{2,3},{3,4},{4,5},{5,6}};

execute it with

    root -l data_cluster.c\(\"runID\"\)
    runID formule like before , like "0124"


# Tracking and calculate effeciency

set chiporder and its contrast first
    
    vector<Int_t> chipID_order = {1,2,3,4,5,6};
    vector<Int_t> chipID_order_contrast = {-1,0,1,2,3,4,5};
chipID_order should fill with chipID in order.

and the following variable, means what chipID's position in chipID_order, for example:

chipID 4's position in chipID_order is 3

If chipID don't exsist in chipID_order, count as -1. chipID 0 don't exsist, so the first position of chipID_order_contrast is -1

It maybe a little complicated, i will find a better expression for it later

change I/O in mainfunc is enough.

# Run in bash
In run.sh u can execute easier by do this:

    eff "0240" &

# Final output file:
    root file is the track selected
    txt file is the effciency


    1	2	3	4	5	6	
    0	0.982044	0.981629	0.977296	0.982442	0

    First line is the chipID
    Second line is its effciency, the first and last chip in geometry won't be calculate.