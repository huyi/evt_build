#include <iostream>
#include <fstream>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include <vector>
#include <unordered_map>
#include <map>
#include "DAC.h"
#include "island.h"
#include "Evt_build.h"
using namespace DAC;
using namespace std;


int main(int argc, char** argv){
    Int_t numOfChip = 6;
    Int_t bias_FPGA = 0;
    Int_t bias_chip = 128;
    vector<Int_t>     chipID  (numOfChip);
    vector<long long>     timeFPGA(numOfChip);
    vector<Int_t>     timeChip(numOfChip);
    vector<Int_t>     row     (numOfChip);
    vector<Int_t>     col     (numOfChip);
    vector<Int_t>     loop     (numOfChip,0);

    TBranch *b_chipID[numOfChip];
    TBranch *b_timeFPGA[numOfChip];
    TBranch *b_timeChip[numOfChip];
    TBranch *b_row[numOfChip];
    TBranch *b_col[numOfChip];

    map<Int_t,pair<long long,Int_t>> curr_time;
    //map<Int_t,Int_t> curr_time_chip;


    Int_t t_eventID = 0;
    Int_t t_numOfHits=0;
    std::vector<Int_t> t_chipID;
    std::vector<Double_t> t_x;
    std::vector<Double_t> t_y;
    std::vector<long long> t_time;
    std::vector<int> t_time_chip;
    std::vector<double> eff(numOfChip,0);

    //string runID = "Run0189/";
    std::vector<std::string> args;
    std::copy(argv + 1, argv + argc, std::back_inserter(args));
    string runID = "Run"+args[0]+"/";
    string str2 = "../../Output/"+runID;
    string str4 = "../../statics/root/"+runID+"Outputhit0";
    
    //string str2 = argv[0];
    //string str4 = argv[1];


    unordered_map<Int_t,string> ip;
    //unordered_map<Int_t,Int_t> chip_order = {{0,4},{1,3},{2,2},{3,5},{4,1}};
    //unordered_map<Int_t,Int_t>  timealign = {{0,0},{1,133},{2,142},{3,212},{4,156}}; //629
    //unordered_map<Int_t,Int_t> chip_order = {{0,1},{1,2},{2,3},{3,4},{4,5}};
    //unordered_map<Int_t,Int_t>  timealign = {{0,146},{1,126},{2,0},{3,90},{4,3}}; //659
    //unordered_map<Int_t,Int_t> chip_order = {{0,4},{1,3},{2,2},{3,1}};
    //unordered_map<Int_t,Int_t>  timealign = {{0,0},{1,4},{2,45},{3,60}}; //733
    unordered_map<Int_t,Int_t> chip_order = {{0,1},{1,2},{2,3},{3,4},{4,5},{5,6}};
    unordered_map<Int_t,Int_t>  timealign = {{0,0},{1,0},{2,0},{3,0},{4,0},{5,0}}; //740
    //string op = str2+"output"+to_string(bias)+".root";
    string op = str2+"output"+to_string(bias_FPGA)+"_"+to_string(bias_chip)+".root";
    for(int i=0;i<=numOfChip;i++){
        ip[i]=str4+to_string(chip_order[i])+".root";
    }

    TFile *input[numOfChip];
    TTree *t1[numOfChip];

    for(Int_t i=0;i<numOfChip;i++){
        input[i] = TFile::Open(ip[i].c_str());
        t1[i]    = (TTree *)input[i]->Get("HitsInfo");
        t1[i]->SetBranchAddress("chipID",   &chipID[i]  ,&b_chipID[i]);
        t1[i]->SetBranchAddress("timeFPGA", &timeFPGA[i],&b_timeFPGA[i]);
        t1[i]->SetBranchAddress("timeChip", &timeChip[i],&b_timeChip[i]);
        t1[i]->SetBranchAddress("row",      &row[i]     ,&b_row[i]);
        t1[i]->SetBranchAddress("col",      &col[i]     ,&b_col[i]);
    }
    
 
    //TFile *output = TFile::Open("../../test.root", "RECREATE"); // output
    TFile *output = TFile::Open(op.c_str(), "RECREATE"); // output
    TTree *t2 = new TTree("test", "title");

    t2->Branch("eventID", &t_eventID);
    t2->Branch("chipID", &t_chipID);
    t2->Branch("x", &t_x);
    t2->Branch("y", &t_y);
    t2->Branch("numOfHits", &t_numOfHits);
    t2->Branch("time", &t_time);
    t2->Branch("time_chip", &t_time_chip);


    //INITIALIZE CURR_TIME
    //unordered_map<Int_t,long long> chip_entry = {{0,22286441},{1,1068830},{2,3001921},{3,4029819}};
    unordered_map<Int_t,long long> chip_entry = {{0,0},{1,0},{2,0},{3,0},{4,0},{5,0}};
    //unordered_map<Int_t,long long> chip_entry = {{0,t1[0]->GetEntries()*0.9},{1,t1[1]->GetEntries()*0.9},{2,t1[2]->GetEntries()*0.9},{3,t1[3]->GetEntries()*0.9},{4,t1[4]->GetEntries()*0.9},{5,t1[5]->GetEntries()*0.9}};
    for(Int_t i = 0; i<numOfChip; i++){
        t1[i]->GetEntry(chip_entry[i]);
        curr_time[i].first = timeFPGA[i];
        curr_time[i].second= (timeChip[i]+timealign[i])%256;
    }
    
    //temp var
    
    pair<long long,Int_t> mintime = min_element(curr_time.begin(),curr_time.end(),[](pair<Int_t,pair<long long,Int_t>> a,pair<Int_t,pair<long long,Int_t>> b){
            if(a.second.first<b.second.first){
                return true;
            }else if(a.second.first>b.second.first){
                return false;
            }else if(a.second.second==b.second.second){
                return a.second.second<b.second.second;
            }
            else{return false;}
            })->second;
    //cout<<mintime.first<<" "<<mintime.second<<endl;
    //for(int i = 0;i<4;i++){
    //    cout<<curr_time[i].first<<"  "<<curr_time[i].second<<" ";
    //}
    //cout<<endl;
    
    Evt_build* e_build = new Evt_build();
    vector<bool> flag(numOfChip,true);
    bool temptoflag=0;
    bool toflag = 1;
    unordered_set<int> fillflag;
    map<string,int> effcount;
    int h4id = 0;
    long long tttime=0;
    string effcount_tag;
    //for(Int_t i=0;i<numOfChip;i++){
    //    for(Int_t j=0;j<t1[i]->GetEntries();j++){
    //        t1[i]->GetEntry(j);
    //        cout<<j<<"/"<<t1[i]->GetEntries()<<endl;
    //    }
    //}
    
    
    while(toflag){
        fillflag.clear();
        effcount_tag.clear();
        for(Int_t i = 0; i < numOfChip; i++){
            if(chip_entry[i]>=t1[i]->GetEntries()){
                flag[i] = false;
                continue;
            }
            while(true){
                if(chip_entry[i]>=t1[i]->GetEntries()){
                    break;
                }
                t1[i]->GetEntry(chip_entry[i]);
                if(timeFPGA[i]<curr_time[i].first) loop[i]++;
                if(e_build->judge(timeFPGA[i],(timeChip[i]+timealign[i])%256,mintime,bias_FPGA,bias_chip)){
                //if(e_build->judge(timeFPGA[i],(timeChip[i]+timealign[i])%256,mintime)){
                    break;
                    //e_build->generate(t_eventID[i],t_chipID[i],t_x[i],t_y[i],t_time[i]);
                    //t_numOfHits = t_x[i].size();
                    //cout<<"Hits: "<<t_numOfHits<<endl; //<<"     Cluster: "<<t_numOfCluster<<endl;
                    //t2->Fill();
                }else{
                    if(fillflag.find(i)==fillflag.end()){
                        fillflag.insert(i);
                        effcount_tag+=to_string(i);
                    }
                    e_build->fill(chip_order[i],row[i],col[i],timeFPGA[i],(timeChip[i]+timealign[i])%256);
                    //cout<<"time "<<timeFPGA[i]<<" "<<(timeChip[i]+timealign[i])%256<<endl;
                    //cout<<"mint "<<mintime.first<<" "<<mintime.second<<endl<<endl;
                }
                chip_entry[i]++;
            }
                         
        }
        effcount[effcount_tag]++;
        e_build->generate(t_eventID,t_chipID,t_x,t_y,t_time,t_time_chip);
        t_numOfHits = t_x.size();
        //t_numOfHits = e_build->getsize();

        if(fillflag.size()>5){
        //if(fillflag.size()>=numOfChip){
            t_eventID = h4id++;
            t2->Fill();
            //for(int i=0;i<t_numOfHits;i++){
            //    cout<<"chipID:"<<t_chipID[i]<<"  ";
            //    cout<<"time: "<<t_time[i]<<endl;
            //    cout<<"timechip: "<<t_time_chip.size()<<endl;
            //    cout<<"t_x:"<<t_x[i]<<"  ";
            //    cout<<"t_y:"<<t_y[i]<<"  "<<endl;
            //}
            cout<<"evt: "<<t_eventID<<endl;
            //for(int i=0;i<numOfChip;i++){
            //    cout<<"entry "<<i<<":  "<<chip_entry[i]<<"time: "<<timeChip[i]<<endl;
            //}
            //cout<<endl;
        }
        

        for(Int_t i = 0; i<numOfChip; i++){
            //cout<<chip_entry[i]<<" ";
            if(chip_entry[i]>=t1[i]->GetEntries()){
                curr_time[i].first  = 9223372036854775807;
                curr_time[i].second = 255;
                continue;
            }
            t1[i]->GetEntry(chip_entry[i]);
            curr_time[i].first  = timeFPGA[i];
            curr_time[i].second = (timeChip[i]+timealign[i])%256;
        }

        mintime = min_element(curr_time.begin(),curr_time.end(),[loop](pair<Int_t,pair<long long,Int_t>> a,pair<Int_t,pair<long long,Int_t>> b){
            if(loop[a.first]>loop[b.first]) return false;

            if(a.second.first<b.second.first){
                return true;
            }else if(a.second.first>b.second.first){
                return false;
            }else if(a.second.second==b.second.second){
                return a.second.second<b.second.second;
            }
            else{return false;}
            })->second;
        
        //for(int i=0;i<numOfChip;i++){
        //    if(curr_time[i].first<mintime.first){
        //        cout<<"warning: "<<i<<" "<<chip_entry[i]<<endl;
        //        cout<<curr_time[i].first<<"  "<<mintime.first<<endl<<endl;
        //    }
        //}
        //if(fillflag.size()>=numOfChip-1){
        //    for(int i = 0;i<numOfChip;i++){
        //        cout<<curr_time[i].first<<"  "<<curr_time[i].second<<" "<<endl;
        //    }
        //    cout<<"mint"<<mintime.first<<" "<<mintime.second<<endl<<endl;
        //}

        temptoflag = 0;
        for(auto tflag:flag){
            //cout<<tflag<<" ";
            temptoflag  = (temptoflag||tflag);
        }
        toflag = temptoflag;
        //cout<<toflag<<endl;
        
        //for(Int_t i=0;i<numOfChip;i++){
        //    cout<<chip_entry[i]<<"/"<<t1[i]->GetEntries()<<"   ";
        //}
        //cout<<endl;

    }
    

    delete e_build;

    output->cd();
    t2->Write();
    output->Close();

    //for(auto& c:effcount){
    //    if(c.first.size()==0) continue;
    //    for(int i=0;i<c.first.size();i++){
    //        cout<<c.first[i]-'0'<<" ";
    //    }
    //    cout<<endl;
    //}

    
    vector<int> ueff(6,0);
    vector<int> deff(6,0);
    for(auto& c:effcount){
        if(c.first.size()==0) continue;
        for(int i=1;i<c.first.size()-1;i++){
            ueff[c.first[i]-'0']+=c.second;
        }
        for(int i=*c.first.begin()-'0'+1;i<*c.first.rbegin()-'0';i++){
            deff[i]+=c.second;
        }
    }
    double u,d;
    for(int i=1;i<numOfChip-1;i++){
        if(deff[i]!=0){
            u = ueff[i];
            d = deff[i];
            eff[i] = u/d;
        }
        cout<<eff[i]<<endl;
    }
    
    ofstream dataFile;
    string optxt = str2+"output"+to_string(bias_FPGA)+"_"+to_string(bias_chip)+".txt";
    dataFile.open(optxt, ofstream::app);
    for(int i=0;i<numOfChip;i++){
        dataFile<<eff[i]<<"\t";
    }
    dataFile.close();

    return 0;
}


//int main(){
//    string str1 = "../../statics/output";
//    string str2 = "../../Output";
//    string str3 = "/Run0579/Run0579-Rundata-analyze-0";
//    string str4 = "/Run0580/Run0580-Rundata-analyze-0";
//    for(int i=2;i<=5;i++){
//        data(str1+str3+to_string(i)+".root",str2+str3+to_string(i)+".root");
//        data(str1+str4+to_string(i)+".root",str2+str4+to_string(i)+".root");
//    }
//    //string ip = str1+str3+to_string(2)+".root";
//    //string op = str2+str3+to_string(2)+".root";
//    return 0;
//}
