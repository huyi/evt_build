CXX = g++
LD  = g++
CFLAGS = -Wall -g

INCS = $(ROOTSYS)/include/ $(PWD)/include . 

LIBS = 'root-config --glibs' 

%.o: %.cpp %.h
	@echo "*"
	@echo "* compile "$@
	@echo "*"
	$(CXX) $(CFLAGS) $(addprefix -I, $(INCS)) -c $< -o $@

test: digit.o constants.o test.o
	@echo "Build test"
	$(LD) $^ $(shell $(ROOTSYS)/bin/root-config --libs) -lGenVector -lGui $(addprefix -L, $(LIBS))  -lm -o $@
    #-lmathlib -lkernlib -lg2c

all : test

clean: 
	\rm *.o	./test





