#include "Riostream.h"
#include "TFile.h"
#include "TTree.h"
#include <sstream>

bool isNumber(const string& str)
{
    for (char const &c : str) {
        if (std::isdigit(c) == 0) return false;
        if (c == ' ') return false;
    }
    return true;
}

void toroot(string input, string output)
{

//open ROOT file to save the TTree in
TFile *ftree = new TFile(output.c_str(),"recreate");

//creation of TTree
TTree *t = new TTree("t", "a simple Tree with struct");

Int_t row,col,time2,chip_ID,valid;
long long time1;
// open the data file for reading
ifstream file;
file.open(input.c_str());
//file.open("./bsrftestbeam/Run0579/Run0579-Rundata-analyze-04.txt");


//creation of branches to hold the variables of the structure

t->Branch("row", &row, "row/I");
t->Branch("col", &col, "col/I");
t->Branch("timeFPGA", &time1);
t->Branch("timechip", &time2, "time2/I");
t->Branch("chipID", &chip_ID, "chip_ID/I");
t->Branch("valid", &valid, "valid/F");

/*
for(Int_t i=0; i <100000; i++)
{
    //read the properties of each particle
    file >> row;
    file >> col;
    file >> time1;
    file >> time2;
    file >> chip_ID;
    file >> valid;
    //fill the tree
    cout<<time1<<endl;
    t->Fill();
}
*/

if(!file){
    cout<<"no file"<<endl;
    return;
}
string line;
while (getline(file, line)){
    //cout<<line<<endl;
    istringstream sin(line); //create string input object
    vector<string> Waypoints;
    string info;
    
    while (getline(sin, info,' '))  
    {
        //cout << "info:" << info<<"  ";
        Waypoints.push_back(info);
    }
    if(!isNumber(Waypoints[1])){
        cout<<"skip"<<endl;
        continue;
    }
    stringstream s1; //transform string to double  
    stringstream s2;         
    stringstream s3;         
    stringstream s4;         
    stringstream s5;         
    stringstream s0;         

    s0 << Waypoints[0];
    s0 >> row;

    s1 << Waypoints[1];
    s1 >> col;
    
    s2 << Waypoints[2];
    s2 >> time1;

    s3 << Waypoints[3];
    s3 >> time2;

    s4 << Waypoints[4];
    s4 >> chip_ID;

    s5 << Waypoints[5];
    s5 >> valid;


    //cout<<row<<" "<<col<<endl;
    t->Fill();

}

file.close();

//write and close the ROOT file
t->Write();
// delete ftree;

}

