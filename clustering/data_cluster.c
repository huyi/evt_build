
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include <vector>
#include "island.h"

using namespace std;

extern const double pixel_sizex;
extern const double pixel_sizey;

//void data_cluster()
void data_cluster(string ids)
{   
    int cores = 1,curr = 0;
    Int_t chipnums = 7;//Use MAX chipID+1
    unordered_map<Int_t,Int_t> chip_order = {{0,1},{1,2},{2,3},{3,4},{4,5},{5,6}};
    Int_t eventID;
    std::vector<Int_t> *planeID = 0;

    std::vector<Double_t> *x = 0;
    std::vector<Double_t> *y = 0;

    TBranch *b_eventID;   //!
    TBranch *b_planeID;   //!
    TBranch *b_x;   //!
    TBranch *b_y;   //!

    Int_t t_eventID;
    Int_t t_numOfhits;
    Int_t t_numOfclusters;
    Int_t t_numOfhitchips;
    std::vector<Int_t> t_planeID_hit;
    std::vector<Int_t> t_planeID_cluster;
    std::vector<Double_t> t_x_local_hit;
    std::vector<Double_t> t_y_local_hit;
    std::vector<Double_t> t_x_local_cluster;
    std::vector<Double_t> t_y_local_cluster;

    std::vector<Int_t> t_planehits(chipnums, 0);
    std::vector<Int_t> t_planeclusters(chipnums, 0);
    std::unordered_map<Int_t,vector<Double_t>> t_planex_local_hit;
    std::unordered_map<Int_t,vector<Double_t>> t_planey_local_hit;


    std::vector<Double_t> tt_x_local_hit;
    std::vector<Double_t> tt_y_local_hit;
    std::vector<Double_t> tt_x_local_cluster;
    std::vector<Double_t> tt_y_local_cluster;

    string strln = "0_128.root";

    string runID = "Run"+ids;
    string direct= "/home/hym/geant4_workspace/Fit/knossos/Output/"+runID+"/output";
    string strln1 = direct+strln;
    TFile *input = TFile::Open(strln1.c_str());
    TTree *t1 = (TTree *)input->Get("test");

    t1->SetBranchAddress("eventID", &eventID, &b_eventID);
    t1->SetBranchAddress("chipID", &planeID);
    t1->SetBranchAddress("x", &x);
    t1->SetBranchAddress("y", &y);

    string str1 = "/home/hym/geant4_workspace/Fit/knossos//Cluster/"+runID+"/cluster"+strln;
    TFile *output = TFile::Open(str1.c_str(), "RECREATE"); // output
    TTree *t2 = new TTree("cluster", "title");
    
    t2->Branch("eventID", &t_eventID);
    t2->Branch("numOfhits", &t_numOfhits);
    t2->Branch("numOfclusters", &t_numOfclusters);
    t2->Branch("numOfhitchips", &t_numOfhitchips);
    t2->Branch("chipID_hit", &t_planeID_hit);
    t2->Branch("chipID_cluster", &t_planeID_cluster); //cluster
    t2->Branch("x_local_hit", &t_x_local_hit);
    t2->Branch("y_local_hit", &t_y_local_hit);
    t2->Branch("x_local_cluster", &t_x_local_cluster); //cluster
    t2->Branch("y_local_cluster", &t_y_local_cluster); //cluster
    t2->Branch("chiphits", &t_planehits);
    t2->Branch("chipclusters", &t_planeclusters);

    Int_t id = 0;
    //TH2F *muti_hit_continuous[maxdraw];

    
    Int_t length = t1->GetEntries();
    Int_t slen = int(length/cores);
    std::unordered_set<int> chipcount;
    //cout<<length<<endl<<slen<<endl;

    for (Int_t i = slen*curr; i < length-slen*(cores-1-curr); i++){
    //for (Int_t i = 0; i <t1->GetEntries(); i++){

        t1->GetEntry(i);
        // temp variable clear and reset
        t_planeID_hit.clear();
        t_planeID_cluster.clear();

        t_x_local_hit.clear();
        t_y_local_hit.clear();

        t_x_local_cluster.clear();
        t_y_local_cluster.clear();

        t_planehits.assign(chipnums, 0);
        t_planeclusters.assign(chipnums, 0);

        chipcount.clear();

        for (int j = 0; j < chipnums; j++)
        {
            t_planex_local_hit[j].clear();
            t_planey_local_hit[j].clear();
        }
        // fill temp var
        for (int j = 0; j < planeID->size(); j++)
        {   
            //if((*Energy_deposition)[j]<200*pow(10,-6)) continue;
            t_planehits[(*planeID)[j]] += 1;
            t_planex_local_hit[(*planeID)[j]].push_back((*x)[j]);
            t_planey_local_hit[(*planeID)[j]].push_back((*y)[j]);
            if(chipcount.find((*planeID)[j])==chipcount.end())   chipcount.insert((*planeID)[j]);
        }

        for (int j = 0; j < chipnums; j++)
        {   
            // temp var to save cluster data
            tt_x_local_hit.clear();
            tt_y_local_hit.clear();
            tt_x_local_cluster.clear();
            tt_y_local_cluster.clear();

            //fill hit data by planeID 
            for (int k = 0; k < t_planex_local_hit[j].size(); k++){
                tt_x_local_hit.push_back(t_planex_local_hit[j][k]);
                tt_y_local_hit.push_back(t_planey_local_hit[j][k]);
            }         
            //cout<<"plane: "<<j<<endl;
            //fill hit data by planeID 
            island *t_island = new island(tt_x_local_hit,tt_y_local_hit);
            vector<double> tt_x_local_cluster = t_island->getcenterxOfislands();
            vector<double> tt_y_local_cluster = t_island->getcenteryOfislands();
            delete t_island;
            

            // Fill processed cluster data
            for (int k = 0; k < tt_x_local_hit.size(); k++)
            {
                t_planeID_hit.push_back(j);
                t_x_local_hit.push_back(tt_x_local_hit[k]);
                t_y_local_hit.push_back(tt_y_local_hit[k]);
                
            }

            for (int k = 0; k < tt_x_local_cluster.size(); k++)
            {
                t_planeID_cluster.push_back(j);
                //cout<<j<<" "<<chip_order[j]<<" ";
                t_x_local_cluster.push_back(tt_x_local_cluster[k]);
                t_y_local_cluster.push_back(tt_y_local_cluster[k]);
            }
            //cout<<endl;
            t_planeclusters[j]=(tt_x_local_cluster.size());
        }
        t_numOfhitchips = chipcount.size();
        t_eventID = eventID;
        t_numOfhits = t_x_local_hit.size();
        t_numOfclusters = t_x_local_cluster.size();
        //cout<<"evt:"<<t_eventID<<endl;
        t2->Fill();
    }
    //cout<<"Part "<<curr<<"over"<<endl;
    input->Close();
    output->cd();
    t2->Write();
    output->Close();
}


