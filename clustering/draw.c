
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include <vector>
#include "island.h"
using namespace std;

extern const double pixel_sizex;
extern const double pixel_sizey;

void draw()
{
    
   // Declaration of leaf types
   Int_t           eventID;
   Int_t           numOfhits;
   Int_t           numOfclusters;
   vector<int>     *chipID_hit     = 0;
   vector<int>     *chipID_cluster = 0;
   vector<double>  *x_local_hit = 0;
   vector<double>  *y_local_hit = 0;
   vector<double>  *x_local_cluster = 0;
   vector<double>  *y_local_cluster = 0;
   vector<int>     *chiphits = 0;
   vector<int>     *chipclusters = 0;

   // List of branches
   TBranch        *b_eventID;   //!
   TBranch        *b_numOfhits;   //!
   TBranch        *b_numOfclusters;   //!
   TBranch        *b_chipID_hit;   //!
   TBranch        *b_chipID_cluster;   //!
   TBranch        *b_x_local_hit;   //!
   TBranch        *b_y_local_hit;   //!
   TBranch        *b_x_local_cluster;   //!
   TBranch        *b_y_local_cluster;   //!
   TBranch        *b_chiphits;   //!
   TBranch        *b_chipclusters;   //!

    TFile *input = TFile::Open("cluster0.root");
    TTree *t1 = (TTree *)input->Get("cluster");

    TFile *output = TFile::Open("draw.root", "RECREATE"); // output
    TTree *t2 = new TTree("draw", "title");

   t1->SetBranchAddress("eventID", &eventID, &b_eventID);
   t1->SetBranchAddress("numOfhits", &numOfhits, &b_numOfhits);
   t1->SetBranchAddress("numOfclusters", &numOfclusters, &b_numOfclusters);
   t1->SetBranchAddress("chipID_hit", &chipID_hit, &b_chipID_hit);
   t1->SetBranchAddress("chipID_cluster", &chipID_cluster, &b_chipID_cluster);
   t1->SetBranchAddress("x_local_hit", &x_local_hit, &b_x_local_hit);
   t1->SetBranchAddress("y_local_hit", &y_local_hit, &b_y_local_hit);
   t1->SetBranchAddress("x_local_cluster", &x_local_cluster, &b_x_local_cluster);
   t1->SetBranchAddress("y_local_cluster", &y_local_cluster, &b_y_local_cluster);
   t1->SetBranchAddress("chiphits", &chiphits, &b_chiphits);
   t1->SetBranchAddress("chipclusters", &chipclusters, &b_chipclusters);

    Int_t id = 0;
    Int_t maxdraw = 10;
    TH2F *muti_hit[maxdraw];
    TH2F *muti_hit_continuous[maxdraw];

    for (int m = 0; m < maxdraw; m++){
        TString nameX = "muti_hit" + to_string(m);
        muti_hit[m] = new TH2F(nameX,"",800,5,15,800,0,10);
        muti_hit[m]->SetMarkerColor(1);
        muti_hit[m]->SetMarkerSize(2);
        muti_hit[m]->SetMarkerStyle(7);
        //muti_hit[m]->SetLineColor(6);
        //muti_hit[m]->SetLineWidth(603);
        TString nameX2 = "muti_cluster" + to_string(m);
        muti_hit_continuous[m] = new TH2F(nameX2,"",800,5,15,800,0,10);
        muti_hit_continuous[m]->SetMarkerColor(1);
        muti_hit_continuous[m]->SetMarkerSize(2);
        muti_hit_continuous[m]->SetMarkerStyle(6);   
        muti_hit_continuous[m]->SetLineWidth(603);   
    }

    for(int i =0;i<maxdraw;i++){
        t1->GetEntry(i);
        for (int k = 0; k < (*x_local_hit).size(); k++){
            //cout<<"hit_0:(x,y) = "<<t_planex_local_hit[j][k]<<","<<t_planey_local_hit[j][k]<<endl;
            //cout<<"hit_1:(x,y) = "<<tt_x_local_hit[k]<<","<<tt_y_local_hit[k]<<endl;
            //cout<<endl;
            muti_hit[i]->Fill((*x_local_hit)[k],(*y_local_hit)[k]);
        }
        for (int k = 0; k < (*x_local_cluster).size(); k++){
            cout<<"cluster:(x,y) = "<<(*x_local_cluster)[k]<<","<<(*y_local_cluster)[k]<<endl;
            muti_hit_continuous[i]->Fill((*x_local_cluster)[k],(*y_local_cluster)[k]);
        }
    }
    output->cd();
    for(int i=0;i<maxdraw;i++){
        muti_hit[i]->Write();
        muti_hit_continuous[i]->Write();

    }
    output->Close();
}


