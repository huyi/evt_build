
#include <vector>
#include <math.h>

using namespace std;
class trackselect{
    private:
    int chipnums;
    vector<vector<int>> chip_order;

    protected:
    std::map<int,vector<int>> chip_clusterID;
    vector<int> repo;

    public:
    trackselect(){
        cout<<"Begin TS"<<endl;
    }
    void generate(vector<int>& planeID);
    const vector<vector<int>>& getorder(){
        return chip_order;
    }
    int getchipnums(){return chipnums;}
    void reset(){
        chip_order.clear();
        chip_clusterID.clear();
        repo.clear();
    }
};
void trackselect::generate(vector<int>& planeID){
    //cout<<planeID.size()<<endl;
    this->reset();
    
    for(int i=0;i<planeID.size();i++){
        chip_clusterID[planeID[i]].push_back(i);
        //cout<< planeID[i]<<" ";
    }
    //cout<<endl;

    chipnums = chip_clusterID.size();
    vector<int> ordernums(chipnums,0);
    int temp = 1;

    /*
    for(int i=chipnums-1;i>=0;i--){
        temp*= chip_clusterID[i+1].size();
        ordernums[i]=temp;
        //cout<<i<<"  "<<temp<<"  "<<chip_clusterID[i+1].size()<<endl;
    }
    chip_order.assign(ordernums[0],vector<int>(chipnums,0));
    for(int j=0;j<chipnums;j++){
        int mnum = ordernums[j]/chip_clusterID[j+1].size();
        for(int i=0;i<ordernums[0];i++){
            chip_order[i][j] = chip_clusterID[j+1][(i/mnum)%chip_clusterID[j+1].size()];
            //cout<<chip_clusterID[j+1].size()<<" ";
            //cout<<i/mnum<<" ";
            //cout<<chip_clusterID[j+1][(i/mnum)%chip_clusterID[j+1].size()]<<" "<<endl;
        }
    }
    */

    
    int flag = chipnums-1;
    for(auto it = chip_clusterID.rbegin();it!=chip_clusterID.rend();++it){
    //for(auto it :chip_clusterID){
        //cout<< it->first <<" "<<it->second.size()<<" "<<endl;
        temp*= it->second.size();
        ordernums[flag--]=temp;
        //cout<<temp<<" ";
    }
    //cout<<endl;
    
    chip_order.assign(ordernums[0],vector<int>(chipnums,0));
    
    

    
    int j = 0;
    for(auto it = chip_clusterID.begin();it!=chip_clusterID.end();++it){
        int mnum = ordernums[j]/it->second.size();
        //cout<<mnum<<" ";
        for(int i=0;i<ordernums[0];i++){
            //cout<<(i/mnum)%it->second.size()<<endl;
            //cout<<it->second[0]<<endl;
            chip_order[i][j] = (it->second)[(i/mnum)%it->second.size()];
        }
        j++;
    }
    

    //for(int i=0;i<ordernums[0];i++){
    //    for(int j=0;j<chipnums;j++){
    //        cout<<chip_order[i][j]<<" ";
    //    }
    //    cout<<endl;
    //}
    //cout<<endl;
}