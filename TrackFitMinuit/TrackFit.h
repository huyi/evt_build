#ifndef TrackFit_h
#define TrackFit_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TRandom.h>
#include <TF1.h>
#include <TRandom3.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
using namespace std; 

class TrackFit {
public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain
  // Declaration of leaf types
  Int_t           TrackSize;
  
  vector<int>     *planeID;
  vector<double>  *xloc;
  vector<double>  *yloc;
  int numOfhits;
  int numOfclusters;
  int numOfhitchips;

  // List of branches
  TBranch        *b_TrackSize;   //!
  TBranch        *b_planeID;   //!
  TBranch        *b_x;   //!
  TBranch        *b_y;   //!
  TBranch        *b_z;   //!
  TBranch        *b_numOfhits;   //!
  TBranch        *b_numOfclusters;   //!
  TBranch        *b_numOfhitchips;   //!

  TrackFit(TTree *tree=0);
  TrackFit(TString input);
  virtual ~TrackFit();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop(TFile * f, TTree* myTree,string path);
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);

  TString InputFile;
};
TrackFit::TrackFit(TTree *tree) : fChain(0) 
{
  InputFile = "/home/hym/geant4_workspace/Fit/knossos/Cluster/Run0162/cluster0.root";
  if (tree == 0) {
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(InputFile);
    if (!f || !f->IsOpen()) {
      f = new TFile(InputFile);
    }
    f->GetObject("cluster",tree);
  }
  Init(tree);
}

TrackFit::TrackFit(TString input) : fChain(0) 
{
  InputFile = input;
  TTree *tree = 0;
  if (tree == 0) {
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(InputFile);
    if (!f || !f->IsOpen()) {
      f = new TFile(InputFile);
    }
    f->GetObject("cluster",tree);
  }
  Init(tree);
}

TrackFit::~TrackFit()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t TrackFit::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}

Long64_t TrackFit::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void TrackFit::Init(TTree *tree)
{
   // Set object pointer
   planeID = 0;
   xloc = 0;
   yloc = 0;

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   //fChain->SetBranchAddress("TrackSize", &TrackSize, &b_TrackSize);
   fChain->SetBranchAddress("chipID_cluster", &planeID, &b_planeID);
   fChain->SetBranchAddress("x_local_cluster", &xloc, &b_x);
   fChain->SetBranchAddress("y_local_cluster", &yloc, &b_y);
   fChain->SetBranchAddress("numOfhits", &numOfhits, &b_numOfhits);
   fChain->SetBranchAddress("numOfclusters", &numOfclusters, &b_numOfclusters);
   fChain->SetBranchAddress("numOfhitchips", &numOfhitchips, &b_numOfhitchips);
   Notify();
}

Bool_t TrackFit::Notify()
{
   return kTRUE;
}

void TrackFit::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}

Int_t TrackFit::Cut(Long64_t entry)
{
  // This function may be called from Loop.
  // returns  1 if entry is accepted.
  // returns -1 otherwise.
  return 1;
}

#endif
