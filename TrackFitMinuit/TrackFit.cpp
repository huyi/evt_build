#define TrackFit_cxx
#include "TrackFit.h"

#include <iostream>
#include <fstream>
#include <TStyle.h>
#include <TCanvas.h>
#include <TMinuit.h>
#include <TMatrix.h>
#include <TMatrixD.h>
#include <TSystem.h>
#include <TNtuple.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include "trackselect.h"

const Int_t npts = 6; // chip nums
// 1 for chi2 fit and 2 for likelyhood fit

double xx[npts], yy[npts], zz[npts]; 
int plane[npts];
double sigmax=0.025/pow(12,0.5), sigmay=0.025/pow(12,0.5); 
vector<Int_t> chipID_order = {1,2,3,4,5,6};
vector<Int_t> chipID_order_contrast = {-1,0,1,2,3,4,5};


void fcn_chisq(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
  const Double_t a1 = par[0];
  const Double_t b1 = par[1];
  const Double_t a2 = par[2];
  const Double_t b2 = par[3];
  
  Double_t chisq=0.; 
  for( int i = 0; i < npts ; i++ ) {
    Double_t chi1  = ( a1*zz[i] + b1 - xx[i] )/sigmax; chi1 = chi1*chi1;   
    Double_t chi2  = ( a2*zz[i] + b2 - yy[i] )/sigmay; chi2 = chi2*chi2;   
    chisq += chi1 +chi2; 
  }	
  
  f = chisq;
  //std::cout << "chisq = " << chisq << std::endl;
}

// initialization and setup 

void Fit(Double_t results[])
{
  TMinuit *gMinuit = new TMinuit(4);  //initialize TMinuit with a maximum of 3 params
  gMinuit->SetFCN(fcn_chisq );
	
  Int_t    ierflg = 0;
  Double_t arglist[10], val[10], err[10];
	
  arglist[0] = -1;
  gMinuit->mnexcm("SET PRI", arglist, 1, ierflg);
  arglist[0] =0.5;
  gMinuit->mnexcm("SET ERR", arglist, 1, ierflg);
	
  // Set starting values and step sizes for parameters
  static Double_t vstart[4] = { 0.0010, 1.,  0.0010, 1.};
  static Double_t   step[4] = { 0.0001, 0.1,  0.0001, 0.1};

  gMinuit->mnparm(0, "a1", vstart[0], step[0], 0, 0, ierflg);
  gMinuit->mnparm(1, "b1", vstart[1], step[1], 0, 0, ierflg);
  gMinuit->mnparm(2, "a2", vstart[2], step[2], 0, 0, ierflg);
  gMinuit->mnparm(3, "b2", vstart[3], step[3], 0, 0, ierflg);

  // Now ready for minimization step
  arglist[0] = 5000;
  arglist[1] = 1.00;
  gMinuit->mnexcm("MIGRAD", arglist , 1, ierflg); // mimimization here ... 
  //gMinuit->mnexcm("MINOS" , arglist , 2, ierflg); // Minos error 
	
  // Print results
  Double_t amin,edm,errdef;
  Int_t nvpar,nparx,icstat;
  gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
  //std::cout<<"nparx="<<nparx<<std::endl;

  TString chnam;
  Double_t xlolim, xuplim;
  Int_t iuext, iuint;
	
  for(Int_t p = 0; p < 4; p++)
  { 
    results[p] =0.0; 
    gMinuit->mnpout(p, chnam, val[p], err[p], xlolim, xuplim, iuint);
    // printf("%2d %5s %8.2f +/- %8.2f\n", p, chnam.Data(), val[p]*1000, err[p]*1000); 
    // printf("%2d %5s %8.2f +/- %8.2f\n", p, chnam.Data(), val[p], err[p]);
    results[p] = val[p]; 
  }
  delete gMinuit; 
}

void TrackFit::Loop(TFile * f, TTree* myTree,string path)
{  
  
   f->cd();
   double Chi2_Out;
   myTree->Branch("Chi2", &Chi2_Out);

   vector<int>* vec_planeOut = new std::vector<int>();
   myTree->Branch("planeID", &vec_planeOut);

   vector<double>* vec_xlocOut = new std::vector<double>();
   myTree->Branch("xloc", &vec_xlocOut);

   vector<double>* vec_ylocOut = new std::vector<double>();
   myTree->Branch("yloc", &vec_ylocOut);
   TH1D * res[npts];
   TH1D * residual[npts][2];

  
   TString namea1 = "a1";
   TString nameb1 = "b1";
   TString namea2 = "a2";
   TString nameb2 = "b2";

   res[0] =  new TH1D(namea1, "", 100, -.001, .001);     
   res[2] =  new TH1D(namea2, "", 100, -.001, .001);
   res[1] =  new TH1D(nameb1, "", 100, -10, 10);
   res[3] =  new TH1D(nameb2, "", 100, -10, 10);

   
   for (int m=0; m < npts; m++){
     TString nameX = "residual_plane"+to_string(m)+"_x";
     TString nameY = "residual_plane"+to_string(m)+"_y";

     //residual[m][0] = new TH1D(nameX, "", 320, -0.8, 0.8);
     //residual[m][1] = new TH1D(nameY, "", 320, -0.8, 0.8);
     residual[m][0] = new TH1D(nameX, "", 100, -0.3, 0.3);
     residual[m][1] = new TH1D(nameY, "", 100, -0.12, 0.12);
   }
  
   Long64_t nentries = fChain->GetEntriesFast();
   double residualX;
   double residualY;
   Double_t results[4]; 
  

   
   trackselect* ts = new trackselect();
   int chipnums = 0;
   std::vector<double> vec_chi2;
   std::vector<double> vec_a1;
   std::vector<double> vec_a2;
   std::vector<double> vec_b1;
   std::vector<double> vec_b2;
   vector<vector<int>> chip_order;
   std::map<string,int> effcount;
   string effcount_tag;
   
   cout<<"Begin"<<endl;
   
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      fChain->GetEntry(jentry);   
      //if(numOfhits!=npts&&numOfhitchips==npts) continue;
      effcount_tag.clear();
      vec_planeOut->clear();
      vec_xlocOut->clear();
      vec_ylocOut->clear();  
     
      if(jentry%100000 == 0) std::cout<<"Processing event " <<jentry<<"..."<<std::endl;

      vec_chi2.clear();
      vec_a1.clear();
      vec_b1.clear();
      vec_a2.clear();
      vec_b2.clear();
      chip_order.clear();

      ts->generate(*planeID);
      
      chip_order.assign(ts->getorder().begin(),ts->getorder().end());
      chipnums = ts->getchipnums();

      for (int i = 0; i < chip_order.size(); i++)
      {
        double chi2 = 0;
        for (int j = 0; j < chipnums; j++)
        {
          plane[j] = planeID->at(chip_order[i][j]);
	        xx[j] = xloc->at(chip_order[i][j]);
	        yy[j] = yloc->at(chip_order[i][j]);
          zz[j] = 20+40*j;      
        } 
        Fit(results);
        for (int k = 0; k < chipnums; k++)
        {
          double dx = results[0]*zz[k] + results[1]-xx[k];
          double dy = results[2]*zz[k] + results[3]-yy[k];
	        chi2 += (dx * dx) / (sigmax*sigmax) + (dy * dy) / (sigmay*sigmay);
        }
        vec_chi2.push_back(chi2);
        vec_a1.push_back(results[0]);
        vec_b1.push_back(results[1]);
        vec_a2.push_back(results[2]);
        vec_b2.push_back(results[3]);
      }
      // for (int i = 0; i < TrackSize; i++) std::cout << "vec_chi2 " << i << " = " << vec_chi2[i]<< std::endl;
      double minChi2 = *min_element(vec_chi2.begin(), vec_chi2.end());
      int minPos = min_element(vec_chi2.begin(), vec_chi2.end()) - vec_chi2.begin();
     
        //std::cout << "minChi2 = " << minChi2 << std::endl;
        //std::cout << "minPos = " << minPos << std::endl;


      if (minChi2 > 15000) continue;
      //eff
      for(int l = 0; l < chipnums; l++){
        effcount_tag+=to_string(planeID->at(chip_order[minPos][l]));
      }
      effcount[effcount_tag]++;
      //find track
      if (chipnums!=npts) continue;
      Chi2_Out = minChi2;
      for (int l = 0; l < npts; l++)
      {
        vec_planeOut->push_back(planeID->at(chip_order[minPos][l]));
        vec_xlocOut->push_back(xloc->at(chip_order[minPos][l]));
        vec_ylocOut->push_back(yloc->at(chip_order[minPos][l]));
       
        residualX = vec_a1[minPos]*zz[l] + vec_b1[minPos] - xloc->at(chip_order[minPos][l]);
        residualY = vec_a2[minPos]*zz[l] + vec_b2[minPos] - yloc->at(chip_order[minPos][l]);
        
        //cout<<vec_a1[minPos]*zz[l] + vec_b1[minPos]-xloc->at(chip_order[minPos][l])<<" ";
        residual[l][0] -> Fill(residualX);
        residual[l][1] -> Fill(residualY);
      }
     myTree->Fill();
     
   }
   myTree->Write();
    
   delete ts;

   for (int j=0; j<npts; j++)
   {
      residual[j][0] -> Fit("gaus", "Q");
      residual[j][1] -> Fit("gaus", "Q");
      residual[j][0] -> Write();
      residual[j][0] -> Print();
      residual[j][1] -> Write();  
   }
  /*
    vector<int> ueff(7,0);
    vector<int> deff(7,0);
    vector<double> eff(7,0);
    for(auto& c:effcount){
        if(c.first.size()==0) continue;
        cout<<c.first<<" "<<c.second<<endl;
        for(int i=1;i<c.first.size()-1;i++){
            ueff[c.first[i]-'0']+=c.second;
        }
        for(int i=*c.first.begin()-'0'+1;i<*c.first.rbegin()-'0';i++){
            deff[i]+=c.second;
        }
    }
    double u,d;
    for(int i=2;i<npts;i++){
        if(deff[i]!=0){
            u = ueff[i];
            d = deff[i];
            eff[i] = u/d;
        }
        cout<<ueff[i]<<" "<<deff[i]<<" "<<u<<" "<<d<<endl;
    }
  */
    map<int,int>   ueff;
    map<int,int>   deff;
    map<int,double> eff;
    vector<int>    temp;
    for(auto& c:effcount){
        if(c.first.size()==0) continue;
        cout<<c.first<<" "<<c.second<<endl;
        temp.clear();
        for(auto& s:c.first){
          temp.push_back(s-'0');
        }
        sort(temp.begin(),temp.end(),[=](const int& a, const int& b){
         if (chipID_order_contrast[a] < chipID_order_contrast[b]) return true;
         else return false;
        });
        for(int i=1;i<temp.size()-1;i++){
            ueff[temp[i]]+=c.second;
        }
        for(int i=chipID_order_contrast[temp[0]]+1;i<chipID_order_contrast[temp[temp.size()-1]];i++){
            deff[chipID_order[i]]+=c.second;
        }
    }
    double u,d;
    for(int i=1;i<npts-1;i++){
        if(deff[chipID_order[i]]!=0){
            u = ueff[chipID_order[i]];
            d = deff[chipID_order[i]];
            eff[chipID_order[i]] = u/d;
        }
        cout<<ueff[chipID_order[i]]<<" "<<deff[chipID_order[i]]<<" "<<u<<" "<<d<<endl;
    }

    ofstream dataFile;
    string optxt = path+".txt";
    dataFile.open(optxt,ofstream::app);
    for(int i=0;i<npts;i++){
        dataFile<<chipID_order[i]<<"\t";
    }
    dataFile<<endl;
    for(int i=0;i<npts;i++){
        dataFile<<eff[chipID_order[i]]<<"\t";
    }
    dataFile<<endl;
    dataFile.close();
    /*
    for (int i=0; i<4; i++){
      // std::cout<<"results"<<i<<":"<<results[i]<<std::endl;
      res[i] -> Fill(results[i]);
    }

    for (int i=0; i<4; i++){
      res[i] -> Fit("gaus", "Q");
      res[i] -> Write();
    }
   
   */  
   //delete residual, res;
   //f->Close(); 
}

int main(int argc, char** argv)
{ 
  std::vector<std::string> args;
  std::copy(argv + 1, argv + argc, std::back_inserter(args));
  string direct = "/home/hym/geant4_workspace/Fit/knossos/TrackFile/";
  string runID  = "Run"+args[0];
  string strln  = "0_128.root";
  string path   = direct+runID+"/test";
  string strln1 = direct+runID+"/test"+strln;
  TFile * f = new TFile(strln1.c_str(), "RECREATE");
  TTree* myTree = new TTree("TrackInfo", "Selected tracks with minimum Chi2");

  string input1 =   "/home/hym/geant4_workspace/Fit/knossos/Cluster/";
  string input2 = input1+runID+"/cluster"+strln;
  TString inp = input2.c_str();
  TrackFit * tf;
  tf= new TrackFit(inp);

  tf->Loop(f, myTree,path);
  f->Close();
  delete tf; 
}