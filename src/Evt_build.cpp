#include "Evt_build.h"
#include "DAC.h"
#include "island.h"
using namespace DAC;

Evt_build::Evt_build(){
    cout<<"Event build Begin"<<endl;
}

Evt_build::~Evt_build(){
    cout<<"Event build End"<<endl;
}

bool Evt_build::judge(int time_FPGA, int time_Chip){
    t_time = toTime(time_FPGA,time_Chip);
    if(time.size()==0) return false;
    if(t_time-time.back()>=time_crit_chip){
        cout<<"Find new event:"<<endl;
        return true;   
    }    
    return false;
}

bool Evt_build::judge(long long time_FPGA, int time_Chip,pair<long long,int> mintime){
    //t_time = toTime(time_FPGA,time_Chip)-time_align;
    ////if(time.size()==0) return false;
    //if(t_time-curr_time>=time_crit){
    //    //cout<<"Find new event:"<<endl;
    //    if(t_time-curr_time==256) return false;
    //    return true;   
    //}    
    //return false;
    if(time_FPGA-mintime.first>time_crit_FPGA+2){
        return true;
    //}else if(abs(time_Chip-mintime.second)<=time_crit_chip){
    //    return false;
    //}else if(abs(abs(time_Chip-mintime.second)-256)<=time_crit_chip){
    //    return false;
    //}else{
    //    return true;
    }else{
        return false;
    }

}

bool Evt_build::judge(long long time_FPGA, int time_Chip,pair<long long,int> mintime,int bias,int bias2){
    FPGA_gap = time_FPGA-mintime.first;//tstamp overflow
    if(FPGA_gap<0){
        FPGA_gap+=(1<<28);
    }
    if(FPGA_gap>time_crit_FPGA+bias){
        return true;
    }else if(abs(time_Chip-mintime.second)<=time_crit_chip+bias2){
        return false;
    }else if(abs(abs(time_Chip-mintime.second)-256)<=time_crit_chip+bias2){
        return false;
    }else{
        return true;
    }
}


void Evt_build::fill(int t_chipID, int row, int col,long long time_FPGA,int time_chip){
    double t_x = 0, t_y = 0;
    toPOS(row,col,t_x,t_y);
    //t_time = toTime(time_FPGA,time_chip);

    chipID.push_back(t_chipID);
    //this->row.push_back(row);
    //this->col.push_back(col);
    x.push_back(t_x);
    y.push_back(t_y);
    time.push_back(time_FPGA);
    this->time_chip.push_back(time_chip);
}

void Evt_build::generate(int& t_evtID,vector<int>& t_chipID,vector<double>& t_x,vector<double>& t_y,vector<long long>& t_time,vector<int>& time_chip){
    //cout<<"Generating Event "<<evtID<<endl;
    //fill event
    t_evtID = evtID++;
    t_chipID.assign(chipID.begin(),chipID.end());

    t_x.assign(x.begin(),x.end());
    t_y.assign(y.begin(),y.end());
    t_time.assign(time.begin(),time.end());
    time_chip.assign(this->time_chip.begin(),this->time_chip.end());
    //clear private
    //chipID.assign(0,0);
    //x.assign(0,0);
    //y.assign(0,0);
    //time.assign(0,0);
    //this->time_chip.assign(0,0);
    chipID.assign(0,0);
    x.assign(0,0);
    y.assign(0,0);
    time.assign(0,0);
    this->time_chip.assign(0,0);

    vector<int>().swap(chipID);
    vector<double>().swap(x);
    vector<double>().swap(y);
    vector<long long>().swap(time);
    vector<int>().swap(this->time_chip);
    //cout << "Capacity = " << t_x.capacity() << endl;
    //cout << "Capacity = " << t_y.capacity() << endl;
    //cout << "Capacity = " << t_time.capacity() << endl;
    //cout << "Capacity = " << time_chip.capacity() << endl<<endl;

}

void Evt_build::generate(int& t_evtID,vector<int>& t_chipID,vector<double>& t_x,vector<double>& t_y,vector<double>& t_x_cluster,vector<double>& t_y_cluster,int& t_numOfCluster,vector<long long>& t_time){
    cout<<"Generating Event "<<evtID<<endl;
    //fill event
    t_evtID = evtID++;
    
    t_chipID.assign(chipID.begin(),chipID.end());

    t_x.assign(x.begin(),x.end());
    t_y.assign(y.begin(),y.end());
    t_time.assign(time.begin(),time.end());

    island *t_island = new island(x,y);
    t_numOfCluster = t_island->getnumOfislands();
    //cout<<t_numOfCluster<<endl;   
    t_x_cluster.assign(t_island->getcenterxOfislands().begin(),t_island->getcenterxOfislands().end());
    t_y_cluster.assign(t_island->getcenteryOfislands().begin(),t_island->getcenteryOfislands().end());
    delete t_island;
    
    
    //clear private
    chipID.clear();
    //row.clear();
    //col.clear();
    x.clear();
    y.clear();
    time.clear();
}

void Evt_build::checkmemory(){
    cout<<"Capacity:  "<<sizeof(chipID)<<endl;
    cout<<"Capacity:  "<<sizeof(x)<<endl;
    cout<<"Capacity:  "<<sizeof(y)<<endl;
    cout<<"Capacity:  "<<sizeof(time)<<endl;
    cout<<"Capacity:  "<<sizeof(time_chip)<<endl<<endl;

}