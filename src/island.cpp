
#include <math.h>
#include <algorithm> 
#include "island.h"
const double EPSINON = 0.00001;

using namespace std;

island::island(const vector<double> &x,const vector<double> &y){
    vector<vector<int>> t_island_id;
    std::unordered_set<int> island_tag;
    //fill pair
    for (int k = 0; k < x.size(); k++){
        t_island.push_back(make_pair(x[k],y[k]));
    }
    //sort from low to high, x first
    sort(t_island.begin(),t_island.end(),[](const pair<double,double>& a, const pair<double,double>& b){
         if (a.first < b.first) return true;
         else if(a.first == b.first){
            return a.second<b.second;
         }
         else return false;
    });
    //fill id vector
    int numid = 0;
    if(t_island.size()!=0) t_island_id.push_back({0});
    for(int i = 1;i<t_island.size();i++){
        if(t_island[i].first!=t_island[i-1].first){
            t_island_id.push_back({i});
            numid++;
        }else{
            t_island_id[numid].push_back(i);
        }
    }
    //find island
    for(int i=0;i<t_island_id.size();i++){
        for(int j=0;j<t_island_id[i].size();j++){
            if(island_tag.find(t_island_id[i][j])==island_tag.end()){
                double x=0;
                double y=0;
                int n=0;
                dfs(island_tag,t_island_id,i,j,x,y,n);
                numOfislands++;
                island_centerx.push_back(x/n);
                island_centery.push_back(y/n);

            }
        }
    }
}

island::~island(){}

void island::dfs(std::unordered_set<int> &t_set,const vector<vector<int>> &id,int i,int j,double &x,double &y,int &n){
    // end situation: over limitation
    if(!inArea(id,i,j))
        return;
    // if checked return
    if(t_set.find(id[i][j])!=t_set.end())
        return;
    t_set.insert(id[i][j]);// tag
    x+= t_island[id[i][j]].first;
    y+= t_island[id[i][j]].second;
    n++;
    
    if(inLink(id,i,j,i,j+1))    dfs(t_set,id,i,j+1,x,y,n);// up   
    if(inLink(id,i,j,i,j-1))    dfs(t_set,id,i,j-1,x,y,n);// down

    if(inArea(id,i-1,0)){
        for(int k = 0;k<id[i-1].size();k++){
            if(inLink(id,i,j,i-1,k))    dfs(t_set,id,i-1,k,x,y,n);// l
        }
    }
    
    if(inArea(id,i+1,0)){
        for(int k = 0;k<id[i+1].size();k++){
            if(inLink(id,i,j,i+1,k))    dfs(t_set,id,i+1,k,x,y,n);// r
        }
    }
    //if(inLink(id,i,j,i+1,j))    dfs(t_set,id,i+1,j,x,y,n);// r
}

bool island::inArea(const vector<vector<int>> &id,int i,int j){
    return i >= 0 && i < id.size() && j >= 0 && j<id[i].size();
}

bool island::inLink(const vector<vector<int>> &id,int i,int j,int ii, int jj){
    if(!inArea(id,i,j)||!inArea(id,ii,jj)) return false;
    double x = t_island[id[i][j]].first-t_island[id[ii][jj]].first;
    double y = t_island[id[i][j]].second-t_island[id[ii][jj]].second;
    //criterion
    return pow(x,2)+pow(y,2) <= pow(pixel_size_x,2)+pow(pixel_size_y,2)+EPSINON;//double data compare need to set EPSINON, IF use ==
}


