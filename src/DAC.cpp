
#include "DAC.h"
#include "math.h"
#include "constants.h"
const double EPSINON = 0.00001;

void DAC::toPOS(int i, int j, double &x, double &y){
    if ((i%4 == 0) || (i%4 == 3)){ j = j*2; }
    else { j = j*2+1; }

    i = i/2;
    y = i*pixel_size_y+pixel_size_y/2-6.4;
    x = j*pixel_size_x+pixel_size_x/2-12.8;
}

void DAC::toID(int &i, int &j, double x, double y){
    i = int((x-pixel_size_x/2+12.8+EPSINON)/pixel_size_x);
    j = int((y-pixel_size_y/2+6.4+EPSINON)/pixel_size_y);

}

long long DAC::toTime(long long FPGA, int Chip){
    return FPGA*FPGA_CLOCK+Chip*Chip_CLOCK;
}